---
title: The Principles of Open Scholarly Infrastructure (v1.1, 2023)
# date: "2023-11-02"
# publishdate: "2020-11-02"
date: "2023-11-03"
lastmod: "2023-11-03"
comments: false
---

**_POSI Version 1.1 Released November 2023_**     
_The POSI [Adopters](/posse)---15 organisations at the time---worked on clarifications to the original principles to create version 1.1 on 3rd November 2023. The new/always-current version is below. See the [marked-up changes with explanations](/posi-v1.1-revisions.pdf) and the archive of the [original version 1.0](/posi-v1.0), for reference._
<br>
<br>

{{< rawhtml >}}
  <div class="well">
{{< /rawhtml >}}

## Governance

- **Coverage across the scholarly enterprise** – research transcends disciplines, geography, institutions, and stakeholders. Organisations and the infrastructure they run need to reflect this.
- **Stakeholder Governed** – a board-governed organisation drawn from the stakeholder community builds confidence that the organisation will take decisions driven by community consensus and a balance of interests.
- **Non-discriminatory participation or membership** – we see the best option as an “opt-in” approach with principles of non-discrimination and inclusivity where any stakeholder group may express an interest and should be welcome. Representation in governance must reflect the character of the community or membership.
- **Transparent governance** – to achieve trust, the processes and policies for selecting representatives to governance groups should be transparent (within the constraints of privacy laws).
- **Cannot lobby** – infrastructure organisations should not lobby for regulatory change to cement their own positions or narrow self-interest. However, an infrastructure organisation’s role is to support its community, and this can include advocating for policy changes.
- **Living will** – a powerful way to create trust is to publicly describe a plan addressing the conditions under which an organisation or service would be wound down. It should include how this would happen and how any assets could be archived and preserved when passed to a successor organisation or service. Any such organisation or service must adopt POSI and honour the POSI principles.
- **Formal incentives to fulfil mission & wind-down** – infrastructures exist for a specific purpose, and that purpose can be radically simplified or even rendered unnecessary by technological or social change. Organisations and services should regularly review community support and the need for their activities. If it is possible, the organisation or service (and staff) should have direct incentives to deliver on the mission and wind down.


## Sustainability

- **Time-limited funds are used only for time-limited activities** – operations are supported by sustainable revenue sources - whereas time-limited funds are used only for time-limited activities. Depending on grants to fund ongoing and/or long-term infrastructure operations fully makes them fragile and distracts from building core infrastructure.
- **Goal to generate surplus** – organisations (or services) that define sustainability based merely on recovering costs are brittle and stagnant. It is not enough to merely survive; organisations and services have to be able to adapt and change. To weather economic, social and technological volatility, they need financial resources beyond immediate operating costs.
- **Goal to create financial reserves** – a high priority should be having ring-fenced financial reserves, separate from operating funds, that can support implementing living will plans, including a complete, orderly wind down or transition to a successor organisation, or major unexpected events.
- **Mission-consistent revenue generation** – revenue sources should be evaluated against the infrastructure’s mission and not run counter to the aims of the organisation or service.
- **Revenue based on services, not data** – data related to the running of the scholarly infrastructure should be community property. Appropriate revenue sources might include value-added services, consulting, API Service Level Agreements or membership fees.

## Insurance

- **Open source** – all software and assets required to run the infrastructure should be available under an open-source licence. This does not include other software that may be involved with running the organisation.
- **Open data (within constraints of privacy laws)** –  For an infrastructure to be forked (reproduced), it will be necessary to replicate all relevant data. The [CC0 waiver](https://creativecommons.org/choose/zero/waiver) is the best practice in making data openly and legally available. Privacy and data protection laws will limit the extent to which this is possible.
- **Available data (within constraints of privacy laws)** – it is not enough that the data be “open” if there is no practical way to obtain it. Underlying data should be made easily available via periodic open data dumps.
- **Patent non-assertion** – the organisation should commit to a patent non-assertion policy or covenant. The organisation may obtain patents to protect its own operations but not use them to prevent the community from replicating the infrastructure.

{{< rawhtml >}}
  </div>
{{< /rawhtml >}}

<br>

_Cite as `Bilder G, Lin J, Neylon C (2020), The Principles of Open Scholarly Infrastructure, retrieved [date], [https://doi.org/10.24343/C34W2H](https://doi.org/10.24343/C34W2H)`_
