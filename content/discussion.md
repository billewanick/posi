---
title: Discussion
subtitle: Community discussion around POSI and an invitation to share your thoughts
date: "2022-11-16"
publishdate: "2022-11-16"
comments: false
---

The following key documents (listed chronologically) discuss or draw on the POSI principles:

| Year & month	| Title	| Authors |
|--------------------------	|---	|---|
| 2015-February	| (Originally-proposed) [Principles for Open Scholarly Infrastructures](http://dx.doi.org/10.6084/m9.figshare.1314859)	| Neylon, Bilder, Lin |
| 2015-August	| [What exactly is infrastructure? Seeing the leopard's spots](http://dx.doi.org/10.6084/m9.figshare.1520432) 	| Neylon, Bilder, Lin |
| 2016-January	| [Where are the pipes? Building Foundational Infrastructures for Future Services](http://cameronneylon.net/blog/where-are-the-pipes-building-foundational-infrastructures-for-future-services/) 	| Neylon, Bilder, Lin |
| 2016-July	| [Squaring Circles: The economics and governance of scholarly infrastructures](http://cameronneylon.net/blog/squaring-circles-the-economics-and-governance-of-scholarly-infrastructures/)	| Neylon |
| 2018-April	| [Supporting Research Communications: A Guide](https://doi.org/10.5281/zenodo.3524663)	| Chodacki et al |
| 2019-January	| [Good Practice Principles for Scholarly Communication Services](https://sparcopen.org/our-work/good-practice-principles-for-scholarly-communication-services/) 	| COAR-SPARC |
| 2019-April	| [EXAMPLARITY CRITERIA for funding from the National Open Science Fund through platforms, infrastructures and editorial content](https://www.ouvrirlascience.fr/examplarity-criteria-for-funding-from-the-national-open-science-fund/) 	| French National Open Science Fund |
| 2020-October	| [Living Our Values and Principles: Exploring Assessment Strategies for the Scholarly Communication Field](https://educopia.org/living-our-values-and-principles/) 	| Skinner and Wipperman, Educopia Institute |
| 2021-October	| [Now is the time to work together toward open infrastructures for scholarly metadata](https://blogs.lse.ac.uk/impactofsocialsciences/2021/10/27/now-is-the-time-to-work-together-toward-open-infrastructures-for-scholarly-metadata/) 	| Hendricks et al |
| 2021-November	| [Beyond open: Key criteria to assess open infrastructure](https://investinopen.org/blog/criteria-to-assess-openinfra/) 	| Invest In Open |
| 2022-February	| [Assessing data infrastructure: the Principles of Open Scholarly Infrastructure](https://blog.ldodds.com/2022/02/23/assessing-data-infrastructure-the-principles-of-open-scholarly-infrastructure/) 	| Dodds |
 

---

All the adoptees welcome questions and suggestions about The Principles of Open Scholarly Infrastructure or about this website.

If your question is not answered in our [FAQ](https://openscholarlyinfrastructure.org/faq/), we encourage you to ask your questions in public using our issue tracker because that allows others who are interested in POSI to comment and contribute to the conversation as well.

[Submit a question and/or suggestion](https://gitlab.com/crossref/posi/-/issues/new)

If you prefer to submit your public question vie email, you can do so simply by clicking on the "Email a new issue to this project" link.

But, If, for whatever reason, you are uncomfortable asking your question in public, then please email questions directly to either:

- john.chodacki [at] ucop [dot] edu
- gbilder [at] crossref [dot] org

... and somebody from POSI will get in touch with you directly.

Thank you.
