---
title: Posse
subtitle: Who has committed to the POSI principles?
date: "2022-03-09"
publishdate: "2021-03-09"
comments: false
---


These organizations or initiatives (listed alphabetically) have formally adopted the POSI principles by publishing an initial self-audit, and committed to routinely demonstrating evidence of following POSI in practice. 


- Crossref: [POSI fan tutte](https://www.crossref.org/blog/posi-fan-tutte/) (2022-March-08) and [original](https://www.crossref.org/blog/crossrefs-board-votes-to-adopt-the-principles-of-open-scholarly-infrastructure) (2020-December-02)
- [CORE](https://blog.core.ac.uk/2022/05/23/core-our-commitment-to-the-principles-of-open-scholarly-infrastructure/) (original posted 2022-May-23)
- [DataCite](https://doi.org/10.5438/vy7h-g464) (original posted 2021-August-30)
- [DOAJ](https://blog.doaj.org/2022/10/06/doaj-commits-to-the-principles-of-open-scholarly-infrastructure-posi/) (original posted 2022-October-06)
- [Dryad](https://blog.datadryad.org/2020/12/08/dryads-commitment-to-the-principles-of-open-scholarly-infrastructure/) (original posted 2020-December-08)
- [Europe PMC](http://blog.europepmc.org/2022/02/EPMC-adopts-POSI.html) (original posted 2022-February-21)
- [JOSS](https://blog.joss.theoj.org/2021/02/JOSS-POSI) (original posted 2021-February-14)
- [Liberate Science](https://libscie.org/principles-of-open-scholarly-infrastructure-2022/) (original posted 2022-August-02)
- [OAPEN & DOAB](https://oapen.hypotheses.org/524) (original posted  2023-May-04)
- [OA Switchboard](https://www.oaswitchboard.org/blog7oct2021) (original posted 2021-October-07)
- [OpenAIRE](https://www.openaire.eu/principles-open-scholarly-infrastructure-openaire-assessment) (original posted 2022-March-29)
- [OpenCitations](https://opencitations.wordpress.com/2021/08/09/opencitations-compliance-with-the-principles-of-open-scholarly-infrastructure/) (original posted 2021-August-09)
- [OurResearch](https://blog.ourresearch.org/posi/) (original posted 2021-June-10)
- [ROR](https://ror.org/blog/2020-12-16-aligning-ror-with-posi/) (original posted 2020-December-16)
- [Sciety](https://blog.sciety.org/open-scholarly-infrastructure/) (original posted 2021-November-22)

> If you are considering adopting POSI and would like to chat about it, please reach out to any colleagues at any of the organizations above.

---

