---
title: About
subtitle: Background and motivation
date: "2022-10-03"
publishdate: "2020-11-20"
comments: false
---

## What

First developed in a [2015 blog post](https://cameronneylon.net/blog/principles-for-open-scholarly-infrastructures/), POSI offers a set of guidelines by which open scholarly infrastructure organisations and initiatives that support the research community can be run and sustained. Others have since built on the foundation of POSI to discuss and propose how all those that support scholarly communications can use these principles to hold each other accountable.  This includes the following:


- Chodacki, J., Cruse, P., Lin, J., & Neylon, C. (n.d.). A Healthy Research Ecosystem: Diversity by Design. In The Winnower. Authorea, Inc. https://doi.org/10.15200/winn.146047.79215
- Chodacki, J., Cruse, P., Lin, J., Neylon, C., Pattinson, D., & Strasser, C. (2018). Supporting Research Communications: a guide (v1.1). Zenodo. https://doi.org/10.5281/ZENODO.3524663
- Lowenberg, D., Chodacki, J., Fenner, M., Kemp, J., & Jones, M. B. (2019). Open Data Metrics: Lighting the Fire (Version 1). Zenodo. https://doi.org/10.5281/ZENODO.3525349
- Cousijn, H., Hendricks, G., & Meadows, A. (2021). Why openness makes research infrastructure resilient. In Learned Publishing (Vol. 34, Issue 1, pp. 71–75). Wiley. https://doi.org/10.1002/leap.1361
- Countless conference presentations, panel discussions, strategic planning meetings, and debates regarding the health of open infrastructure

## Why

This site codifies POSI as a community resource. Many open infrastructure organisations are at different stages of their journeys toward compliance with POSI. As more attention is paid to open infrastructure, we've missed having a place where POSI can be more closely integrated into the community's vision for scholarly research.

This site therefore provides a reference point for organisations that are discussing and striving to meet POSI. It also aims to address some commonly-heard misconceptions around what is "open" and what is "infrastructure" via the [FAQs](/faq/).


## Who

Any organisation that provides open infrastructure for research and scholarly communications is welcome; this is not an exclusive club. The site was set up by the original blog post authors and collaborators who have contributed to and built upon POSI. While the website is hosted by [Crossref](https://crossref.org), individuals from [California Digital Library](https://cdlib.org), [DataCite](https://datacite.org), [eLife](https://elifesciences.org), and [Dryad](https://datadryad.org/) have so far been involved in this site. To contact us, please open a [GitLab issue](https://gitlab.com/crossref/posi/-/issues).


## How

Any initiative can self-identify as open infrastructure and pointing to this site allows anyone to make a public statement of intent and to invite community appraisal. If you would like to cite POSI, please use: `Bilder G, Lin J, Neylon C (2020), The Principles of Open Scholarly Infrastructure, retrieved [date], https://doi.org/10.24343/C34W2H `

For now this website is simply a home for the principles. In the future, it could be a place where the community records POSI adopters or even measures progress. If you'd like to share experiences with POSI or ideas about what this site could achieve more broadly, please open a [GitLab issue](https://gitlab.com/crossref/posi/-/issues) for others to comment on.
